/**
 * Crie uma função que receba um objeto com N propriedades com um valor Integer  x (Integer Map), e retorne o objeto com as mesmas propriedades, porém com valores remapeados para a divisao 100 / x.
 *
 * Obs.: Propriedades com valores iguais a 0 devem ser remapeadas para o valor `Infinity` (caso a linguagem tenha) ou para a string `'Infinity'` (caso não tenha suporte).
 *
 * Exemplos válidos:
 * { a: 5, b: 4 } -> { a: 20, b: 25 }
 * { a: 1, b: 2, c: 0, d: 20 } -> { a: 100, b: 50, c: Infinity, d: 5 }
 * { foo: 25, bar: 0, baz: 100 } -> { foo: 4, bar: 'Infinity', baz: 1 }
 */

const inp = { a: 7, b: 16, c: 0, d: -5, e: 200 }
const fDivision = number => { return 100 / number }

const oMap = (oObj, fCallback) => {
    const oDummy = {...oObj}
    const oKeys = Object.keys(oDummy)
    oKeys.map( key => { oDummy[key] = fCallback(oDummy[key]) })
    return oDummy
}

function oMap2(oObj, fCallback) {
    const oDummy = {...oObj}
    for (key in oDummy) {
        oDummy[key] = fCallback(oDummy[key])
    }
    return oDummy
}

console.log(inp)
console.log(oMap(inp, fDivision))
console.log(oMap2(inp, fDivision))
console.log(inp)