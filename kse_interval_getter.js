class YearMonth {
    constructor (month, year){
        this.month = month
        this.year = year
    }

    get asString () {
        const monthDict = [
            "Jan", "Fev", "Mar", "Abr",
            "Mai", "Jun", "Jul", "Ago",
            "Set", "Out", "Nov", "Dez"
        ]
        
        const sYear = String(this.year),
            sMonth = monthDict[this.month]

        return sMonth + "/" + sYear
    }

    get asStamplike (){ return this.year * 100 + this.month }

    getVariationInMonths (oYearMonth) {
        const yearVariation = oYearMonth.year - this.year
        
        return oYearMonth.month - this.month + 12 * yearVariation
    }
    
    addMonths(nMonths){
        const newMonth = (this.month + nMonths) % 12,
            newYear = this.year + Math.floor((this.month + nMonths)/12)
        
        return new YearMonth(newMonth, newYear)
    }

    isBetween(YMStart, YMEnd){
        const isAfterStart = YMStart.asStamplike <= this.asStamplike,
            isBeforeEnd = this.asStamplike <= YMEnd.asStamplike
        
        return isAfterStart && isBeforeEnd
    }

}

const getLastUpdate = (firstUpdate, currentDate, Freq)=>{

    function getFirstNeededEval(firstUpdate, currentDate, Freq) {
        /**
         *    Usa o fato que, para qualquer frequência de atualização, eventualmente a atualização vai ser feita no mesmo mês,
         * com isso, pode-se usar quantos anos se passam para retornar ao mesmo mês para chegar mais próximo da data atual sem
         * precisar checar se a data atual está nesse período.
         *
         * Baboseira nerd: 
         *     Voltar ao mesmo mês, chamado M0, depois de um certo # de meses passado, chamado mesesParaRetorno, pode ser descrito como
         * (mesesParaRetorno + M0) % 12 === M0, o que quer dizer que: mesesParaRetorno % 12 === 0.
         * Enquanto isso, sabendo a frequência de atualização, chamada de Freq, e buscando saber o menor # de atualizações antes de se
         * retornar ao M0, chamado de atlNum.
         * pode-se notar que mesesParaRetorno = Freq * atlNum, com isso: (Freq * atlNum) % 12 === 0
         * propriedade de módulo: atlNum = 12/MDC(Freq, 12)
         * Obs: atlNum é um desses: [1, 2, 3, 4, 6, 12], então o maior número possível de atualizações que se tem que olhar é 12
         * 
         * tl;dr confia
         */
        function getMDCwith12(numb) {
            let mdc = 1
            const isDivisible = (denominator) => { return numb % denominator === 0 }
            if (isDivisible(4)){
                mdc *= 4
            }else if(isDivisible(2)){
                mdc *= 2
            }
            if (isDivisible(3)){
                mdc *= 3
            }
            return mdc
        }

        // TODO func
        const numerosDeAtualizações = 12/getMDCwith12(Freq)

        // TODO func
        const mesesParaRetorno = numerosDeAtualizações * Freq
        const mesesTranscorridos = firstUpdate.getVariationInMonths(currentDate)
        const numRetornos = Math.floor(mesesTranscorridos/mesesParaRetorno)
    
        return firstUpdate.addMonths(numRetornos * mesesParaRetorno)
    }

    const firstNeededEval = getFirstNeededEval(firstUpdate, currentDate, Freq)

    // TODO let PS
    for(let evalIndex = 0; evalIndex < 12; evalIndex++){
        // TODO let PE = PS + Freq - 1
        const monthsToThisEval = Freq * evalIndex,
            monthsToNextEval = monthsToThisEval + Freq,
            PeriodStart = firstNeededEval.addMonths(monthsToThisEval),
            PeriodEnd = firstNeededEval.addMonths(monthsToNextEval - 1)
        
        if (currentDate.isBetween(PeriodStart, PeriodEnd)) return PeriodStart 
    }
}

const formatOutput = (firstUpdate, currentDate, updateFrequency, lastUpdate)=> {
    return `já que a primeira atualização foi em ${firstUpdate.asString} atualizando a cada ${updateFrequency} meses o dado usado em ${currentDate.asString} foi atualizado pela última vez em  foi atualizado pela última vez em ${lastUpdate.asString}`
}

//gerando 'inputs'
const firstUpdate = new YearMonth(1, 1998),
    currentDate = new YearMonth(0, 2025),
    updateFrequency = 9

//gerando e formatando o output
const lastUpdate = getLastUpdate(firstUpdate, currentDate, updateFrequency),
    outputStr = formatOutput(firstUpdate, currentDate, updateFrequency, lastUpdate)

console.log(outputStr)