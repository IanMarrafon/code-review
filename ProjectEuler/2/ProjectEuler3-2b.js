/* Get the sum of all fibonacci numbers that are even and smaller than 4 million
 *
 * Fibonacci starts with 1, 1. So that means it starts with Odd, Odd
 * 
 * Known:
 * Even + Even = Odd
 * Odd + Odd = Even
 * Even + Odd = Odd
 * Odd + Even = Odd
 *
 * So, in Fibbo:
 * O, O => E
 * O, O, E => O
 * O, O, E, O => O
 * O, O, E, O, O => E
 * 
 * Evens are distributed in every 3rd element
 */
const nUpperLimit = 4 * 10**6

let nSum = 0
let aFibonacci = [1, 1]
let nCycleCount = 2

const iterNextFibonacci = ([a, b]) => [b, a + b]
do {
    aFibonacci = iterNextFibonacci(aFibonacci)

    nCycleCount = (nCycleCount + 1) % 3
    if (nCycleCount === 0) nSum += aFibonacci[1]
} while (aFibonacci[1] < nUpperLimit);

console.log(nSum)