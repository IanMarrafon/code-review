/* Get the sum of all fibonacci numbers that are even and smaller than 4 million
 *
 * Fibonacci starts with 1, 1. So that means it starts with Odd, Odd
 * 
 * Known:
 * Even + Even = Odd
 * Odd + Odd = Even
 * Even + Odd = Odd
 * Odd + Even = Odd
 *
 * So, in Fibbo:
 * O, O => E
 * O, O, E => O
 * O, O, E, O => O
 * O, O, E, O, O => E
 * 
 * Evens are distributed in every 3rd element
 */

const getEvenFibonacciSumUntil = (nLimit, nSum = 0, aFibonacci = [1, 1, 2]) => {
    const [a, b, c] = aFibonacci
    if (c > nLimit) {
        return nSum
    } else {
        const nNewSum = nSum + c
        const aNextFibonacci = [b + c, b + 2*c, 2*b + 3*c]
        return getEvenFibonacciSumUntil(nLimit, nNewSum, aNextFibonacci)
    }
}

const nReturn = getEvenFibonacciSumUntil(4 * 10**6)
console.log(nReturn)