// Get the sum of all fibonacci numbers that are even and smaller than 4 million
const nUpperLimit = 4 * 10**6

let nSum = 0
let aFibonacci = [1, 1]

const getLastFibonacci = aFibonacci => aFibonacci[1]
const iterNextFibonacci = ([a, b]) => [b, a + b]
do {
    aFibonacci = iterNextFibonacci(aFibonacci)

    if (aFibonacci[1] % 2 === 0) nSum += aFibonacci[1]
} while (aFibonacci[1] < nUpperLimit);

console.log(nSum)