/*
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
const nMaxDivisor = 20
const aDivisors = Array.from({length: nMaxDivisor}, (_, i) => i + 1)

const fAddNextFactor = (aFactors, nDivisor) => {
    const fDivideIfPossible = (nDividend, nDivisor) => {
        const isDivisible = nDividend % nDivisor === 0
        return isDivisible ? nDividend / nDivisor : nDividend
    }
    const nNextFactor = aFactors.reduce(fDivideIfPossible, nDivisor)
    aFactors.push(nNextFactor)
    return aFactors
}
const aFactors = aDivisors.reduce(fAddNextFactor, [])

const fProductOf = (a, b) => a*b
const nResult = aFactors.reduce(fProductOf, 1)

console.log(nResult)