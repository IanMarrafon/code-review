/*
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */

const nMaxDivisor = 20
const aDivisors = Array.from({length: nMaxDivisor}, (_, i) => i + 1)

const aPrimes = [2, 3, 5, 7, 11, 13, 17, 19]

const aFactors = aPrimes.map(nPrime => {
    const nMaxExponent = Math.floor(Math.log(nMaxDivisor)/Math.log(nPrime))
    return nPrime**nMaxExponent
})

const fProductOf = (a, b) => a*b
const nResult = aFactors.reduce(fProductOf, 1)

console.log(nResult)