/*
 *  find biggest palindrome number from a product of two 3-digit numbers
 *  number range: 100²:999² => 10000:998001 => number is in form ABCCBA OR ABCBA
 *
 */

const getBiggest3DigitPalindrome = () => {
    const asTwo3DigitNumbers = (numb) => {
        has3Digits = (numb) => 100<=numb && numb<=999 
        for (let p = 100; p<= 999; p++) {
            const isDivisible = numb % p === 0
            const q = numb / p
            if (isDivisible && has3Digits(q)) return [p, q]
        }
        return
    }
    const as6DigitPalindrome = nSeed => {
        const sSeed = String(nSeed)
        const aFirstPart = sSeed.split("")
        const sFirstPart = sSeed
        const aSecondPart = aFirstPart.reverse()
        const sSecondPart = aSecondPart.join("")
        const sPalindrome = sFirstPart + sSecondPart
        const nPalindrome = Number(sPalindrome)
        return nPalindrome
    }
    const as5DigitPalindrome = nSeed => {
        const sSeed = String(nSeed)
        const aFirstPart = sSeed.split("")
        const sMidPart = aFirstPart.pop()
        const aSecondPart = aFirstPart.reverse()
        const sSecondPart = aSecondPart.join("")
        const sFirstPart = aFirstPart.join("")
        const sPalindrome = sFirstPart + sMidPart + sSecondPart
        const nPalindrome = Number(sPalindrome)
        return nPalindrome
    }

    for (let nSeed = 999; nSeed >= 100; nSeed--){
        const nPalindrome = as6DigitPalindrome(nSeed)
        const aDivisors = asTwo3DigitNumbers(nPalindrome)
        if (aDivisors !== undefined) return `${aDivisors[0]} x ${aDivisors[1]} = ${nPalindrome}`
    }
    for (let nSeed = 999; nSeed >= 100; nSeed--){
        const nPalindrome = as5DigitPalindrome(nSeed)
        const aDivisors = asTwo3DigitNumbers(nPalindrome)
        if (aDivisors !== undefined) return `${aDivisors[0]} x ${aDivisors[1]} = ${nPalindrome}`
    }
    return "Found No Number"
}

console.log(getBiggest3DigitPalindrome())