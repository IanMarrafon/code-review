const nInput = 600851475143

// Biggest Boi Version
const findLargestDivisor = (nEval) => {
    const aDivisors = []
    const aQuotients = []
    for(let i = 1; i <= Math.sqrt(nEval); i++) {
        if (nEval % i === 0) {
            aDivisors.unshift(i) // will increase with each iter
            aQuotients.push(nEval/i)  // will decrease with each iter
        }
    }

    // remove 1, because it is not a prime factor.
    aDivisors.pop()

    // this way aFactors will be in decreasing order
    const aFactors = [...aQuotients, ...aDivisors]

    const nSelfEvalValue = aFactors.shift()

    // nEval has no factors besides itself <=> nEval is prime
    const isEvalPrime = aFactors.length === 0
    if (isEvalPrime) {
        return nSelfEvalValue
    } else {
        const getAllPrimesUntil = nLimit => {
            const isPrime = (nEval, aSmallerPrimes) => {
                for (let p of aSmallerPrimes) {
                    if (p > Math.sqrt(nEval)) break
                    if (nEval % p === 0) return false
                }
                return true
            }
            const aPrimes = []
            
            for (let i = 2; i <= nLimit; i++) {
                if (isPrime(i, aPrimes)) aPrimes.push(i)
            }
            return aPrimes
        }
        
        const nBiggestFactor = aFactors[0]

        const aPrimes = getAllPrimesUntil(nBiggestFactor)

        for (let factor of aFactors) {
            if (aPrimes.includes(factor)) return factor
        }
    }
}

const result = findLargestDivisor(nInput)