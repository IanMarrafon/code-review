const nInput = 600851475143

// Big Boi Version
const findLargestDivisor = (nEval) => {
    const getArrPrimes = (nLimit) => {
        const isPrime = (nEval, aSmallerPrimes) => {
            for (let p of aSmallerPrimes) {
                if (nEval % p === 0) return false
            }
            return true
        }
        const aPrimes = []
        
        for (let i = 2; i <= nLimit; i++) {
            if (isPrime(i, aPrimes)) aPrimes.push(i)
        }
        return aPrimes

    }
    
    const nSqrt = Math.sqrt(nEval)
    const aDivisors = []
    const aQuotients = []
    const aPrimes = getArrPrimes(nEval)
    for(let i = 1; i <= nSqrt; i++) {
        if (nEval % i === 0) {
            aDivisors.push(i) // will increase with each iter
            aQuotients.push(nEval/i)  // will decrease with each iter
        }
    }

    // this way aFactors will be in decreasing order
    const aFactors = [...aQuotients, ...aDivisors.reverse()]
    for (let factor of aFactors) {
        if (aPrimes.includes(factor)) return factor
    }
}

const result = findLargestDivisor(nInput)