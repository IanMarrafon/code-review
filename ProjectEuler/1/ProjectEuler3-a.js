const nInput = 600851475143

const findLargestDivisor = (nEval) => {
    const isPrime = x => {
        for (let i = 2; i < x; i++) {
            if (x % i === 0) return false
        }
        return true
    }

    const nSqrt = Math.sqrt(nEval)
    const aDivisors = []
    const aQuotients = []
    for(let i = 1; i <= nSqrt; i++) {
        if (nEval % i === 0) {
            aDivisors.push(i) // will increase with each iter
            aQuotients.push(nEval/i)  // will decrease with each iter
        }
    }

    // this way aFactors will be in decreasing order
    const aFactors = [...aQuotients, ...aDivisors.reverse()]
    for (let factor of aFactors) {
        if (isPrime(factor)) return factor
    }
}

const result = findLargestDivisor(nInput)