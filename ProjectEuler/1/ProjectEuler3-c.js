const nInput = 600851475143

// Biggest Boi Version
const findLargestDivisor = (nEval) => {
    const getArrPrimes = (nLimit) => {
        const isPrime = (nEval, aSmallerPrimes) => {
            for (let p of aSmallerPrimes) {
                if (nEval % p === 0) return false
            }
            return true
        }
        const aPrimes = []
        
        for (let i = 2; i <= nLimit; i++) {
            if (isPrime(i, aPrimes)) aPrimes.push(i)
        }
        return aPrimes

    }

    const isInPrimeArr = (nTest, nIndex, aPrimes) => {
        if (nTest <= aPrimes[nIndex]) nIndex--
        return nTest === aPrimes[nIndex]
    }
    
    const nSqrt = Math.sqrt(nEval)
    const aDivisors = []
    let nEvaluatedPrimeIndex = aPrimes.length
    const aPrimes = getArrPrimes(nEval)
    for(let i = 1; i <= nSqrt; i++) {
        if (nEval % i === 0) {
            aDivisors.push(i)
            const quot = nEval/i
            if (isInPrimeArr(quot, nEvaluatedPrimeIndex, aPrimes)) return quot
        }
    }

    // iter in aDivisors backwards
    for (let i = aDivisors.length; i>= 0; i--){
        if (isInPrimeArr(aDivisors[i], nEvaluatedPrimeIndex, aPrimes)) return aDivisors[i]
    }
}

const result = findLargestDivisor(nInput)